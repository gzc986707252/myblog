package xyz.zchao.blog.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class Tag {
    private Integer tagId;

    @NotNull(message = "标签名不能为空")
    @Length(max = 10, message = "标签名不能超过10个字符")
    private String tagName;

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName == null ? null : tagName.trim();
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                '}';
    }
}