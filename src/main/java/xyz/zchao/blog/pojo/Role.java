package xyz.zchao.blog.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class Role {
    private Integer roleId;

    @NotNull(message = "角色名不能为空")
    @Length(max = 10, message = "角色名不能超过10个字符")
    private String roleName;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}