package xyz.zchao.blog.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class Link {
    private Integer linkId;

    @NotNull(message = "链接名称不能为空")
    @Length(max = 20, message = "链接名称不能超过20个字符")
    private String linkName;

    @NotNull(message = "链接url不能为空")
    @Length(max = 100, message = "链接url不能超过100个字符")
    private String url;

    public Integer getLinkId() {
        return linkId;
    }

    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName == null ? null : linkName.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }
}