package xyz.zchao.blog.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class Category {
    private Integer categoryId;

    @NotNull(message = "分类名不能为空")
    @Length(max = 10, message = "分类名不能超过10个字符")
    private String categoryName;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}