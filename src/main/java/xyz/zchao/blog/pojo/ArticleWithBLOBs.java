package xyz.zchao.blog.pojo;

public class ArticleWithBLOBs extends Article {
    private String contentHtml;

    private String contentMd;

    public String getContentHtml() {
        return contentHtml;
    }

    public void setContentHtml(String contentHtml) {
        this.contentHtml = contentHtml == null ? null : contentHtml.trim();
    }

    public String getContentMd() {
        return contentMd;
    }

    public void setContentMd(String contentMd) {
        this.contentMd = contentMd == null ? null : contentMd.trim();
    }

    @Override
    public String toString() {
        return "ArticleWithBLOBs{" +
                super.toString() +
                "contentHtml='" + contentHtml + '\'' +
                ", contentMd='" + contentMd + '\'' +
                '}';
    }
}