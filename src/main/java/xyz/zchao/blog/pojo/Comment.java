package xyz.zchao.blog.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Comment {
    private Integer commentId;

    @NotNull(message = "content不能为空")
    private String content;

    private Integer userId;

    private Integer articleId;

    private Integer parentId;

    private Date commentTime;

    //发表评论的用户的简要信息
    private Map<String, Object> fromUser;
    //回复的用户简要信息
    private Map<String, Object> toUser;
    //如果是评论文章，则存放文章的简要信息
    private Map<String, Object> article;
    //子评论列表
    private List<Comment> childNode;

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public Map<String, Object> getFromUser() {
        return fromUser;
    }

    public void setFromUser(Map<String, Object> fromUser) {
        this.fromUser = fromUser;
    }

    public Map<String, Object> getToUser() {
        return toUser;
    }

    public void setToUser(Map<String, Object> toUser) {
        this.toUser = toUser;
    }

    public Map<String, Object> getArticle() {
        return article;
    }

    public void setArticle(Map<String, Object> article) {
        this.article = article;
    }

    public List<Comment> getChildNode() {
        return childNode;
    }

    public void setChildNode(List<Comment> childNode) {
        this.childNode = childNode;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                ", articleId=" + articleId +
                ", parentId=" + parentId +
                ", commentTime=" + commentTime +
                ", fromUser=" + fromUser +
                ", toUser=" + toUser +
                ", article=" + article +
                ", childNode=" + childNode +
                '}';
    }
}