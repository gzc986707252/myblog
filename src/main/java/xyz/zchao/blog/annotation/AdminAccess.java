package xyz.zchao.blog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description: 管理员访问注解
 * @Author: guozongchao
 * @Date: 2020/5/17 17:22
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AdminAccess {
    /**
     * 角色代码
     *
     * @return
     */
    String[] code() default {};
}
