package xyz.zchao.blog.util;

/**
 * 响应结果状态码
 */
public enum ResultCode {
    SUCCESS(0, "操作成功"),
    FAILED(1001, "操作失败"),
    VALIDATE_FAILED(1002, "参数校验错误"),
    USER_NOT_FOUND(1003, "用户不存在"),
    ACCESS_DENIED(1004, "访问拒绝"),
    USER_NOT_LOGGED_IN(1005, "请先登录"),
    TOKEN_EXPIRED(1006, "token已过期，请重新登录！"),
    TOKEN_SIGNATURE_ERROR(1007, "token签名错误"),
    TOKEN_INVALID(1008, "token校验错误,请重新登录"),
    USER_INCONSISTENT(1007, "请求用户与签名用户不一致"),
    UPLOAD_ERROR(1008, "上传错误"),
    ENCRYPT_DECRYPT_ERROR(1009, "加解密出错"),
    NO_RESULT(10010, "查询不到记录"),
    MISSING_REQUEST_PARAMETER(1011, "请求参数缺失"),
    UNKNOWN_ERROR(-1, "未知错误");
    private int code;
    private String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
