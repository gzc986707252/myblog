package xyz.zchao.blog.util;

public class PageResultVO<T> extends ResultVO<T> {

    //数据总量
    private Long count;


    public PageResultVO(Integer code, String msg, T data, Long count) {
        super(code, msg, data);
        this.count = count;
    }

    public PageResultVO(ResultCode resultCode, T data, Long count) {
        super(resultCode, data);
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
