package xyz.zchao.blog.util;

/**
 * 统一JSON数据格式工具
 */
public class ResultVO<T> {
    //响应状态码
    private int code;
    //响应信息
    private String msg;
    //响应的具体数据
    private T data;

    public ResultVO(int code,String msg,T data){
        this.code=code;
        this.msg=msg;
        this.data=data;
    }

    public ResultVO(ResultCode resultCode, T data) {
        this.code=resultCode.getCode();
        this.msg=resultCode.getMsg();
        this.data=data;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }


    @Override
    public String toString() {
        return "ResultUtil{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
