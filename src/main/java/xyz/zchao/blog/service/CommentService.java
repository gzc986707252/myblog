package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Comment;

import java.util.List;

/**
 * @Description: 评论留言业务类
 * @Author: guozongchao
 * @Date: 2020/5/10 12:42
 */
public interface CommentService extends BaseService<Comment> {

    /**
     * 获取所有父评论
     * 参数article如果为不null,则获取对应文章的父评论
     * 否则，只获取网站的留言评论
     * @param articleId
     * @return
     */
    List<Comment> getParentComments(Integer articleId);
}
