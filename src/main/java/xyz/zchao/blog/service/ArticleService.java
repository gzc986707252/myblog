package xyz.zchao.blog.service;

import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 18:24
 */
public interface ArticleService extends BaseService<ArticleWithBLOBs> {
    /**
     * 根据参数获得文章(包含正文)列表
     *
     * @param param
     * @return
     */
    List<ArticleWithBLOBs> findArticlesByOptionalParametersWithBLOBs(ArticleParam param);

    /**
     * 根据参数获得文章(不包含正文)列表
     *
     * @param param
     * @return
     * @see ArticleParam 查看可用参数
     */
    List<Article> findArticlesByOptionalParameters(ArticleParam param);

    ArticleWithBLOBs findArticleByIdWithBLOBs(Integer id);

    Article findArticleById(Integer id);

}
