package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Diary;

/**
 * @Description: 日记业务接口
 * @Author: guozongchao
 * @Date: 2020/5/7 15:20
 */
public interface DiaryService extends BaseService<Diary>{

}
