package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.zchao.blog.dao.DiaryMapper;
import xyz.zchao.blog.pojo.Diary;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.service.DiaryService;
import xyz.zchao.blog.service.UserService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @Description: 日记业务实现类
 * @Author: guozongchao
 * @Date: 2020/5/7 15:39
 */
@Service
@Transactional
public class DiaryServiceImpl implements DiaryService {
    @Resource
    private DiaryMapper diaryMapper;
    @Resource
    private UserService userService;
//    @Resource
//    private ObjectMapper objectMapper;

    @Override
    public int create(Diary record) {
        return diaryMapper.insertSelective(record);
    }

    @Override
    public Diary retrieveById(Integer id) {
        return diaryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return diaryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Diary record) {
        record.setUpdateTime(new Date());
        return diaryMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return diaryMapper.getCount();
    }

    @Override
    public List<Diary> retrieveAllByPaging(Integer page, Integer limit) {
        List<Diary> diaries = diaryMapper.selectAllByPaging(page, limit);
        diaries.stream().forEach(diary -> {
            User user=userService.retrieveById(diary.getUserId());
            diary.setUser(new HashMap<>());
            diary.getUser().put("userId",user.getUserId());
            diary.getUser().put("userName",user.getUserName());
        });
        return diaries;
    }
}
