package xyz.zchao.blog.service.impl;

import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.zchao.blog.dao.ArticleMapper;
import xyz.zchao.blog.dao.ArticleTagsMapper;
import xyz.zchao.blog.dao.CategoryMapper;
import xyz.zchao.blog.dao.UserMapper;
import xyz.zchao.blog.exception.CustomizeException;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;
import xyz.zchao.blog.pojo.Tag;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.service.ArticleService;
import xyz.zchao.blog.util.ResultCode;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 18:32
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private ArticleTagsMapper articleTagsMapper;
    @Resource
    private UserMapper userMapper;


    @Override
    public List<ArticleWithBLOBs> findArticlesByOptionalParametersWithBLOBs(ArticleParam param) {
        //开启分页
        PageHelper.startPage(param == null ? 1 : param.getPage(), param == null ? 0 : param.getLimit());
        List<ArticleWithBLOBs> articles = articleMapper.selectArticlesByOptionalParametersWithBLOBs(param);
        //查找文章对应的用户信息，并填充到相关属性
        articles.stream().forEach(article -> {
            User user = userMapper.selectByPrimaryKey(article.getUserId());
            article.setUser(new HashMap<>());
            article.getUser().put("userId", user.getUserId());
            article.getUser().put("userName", user.getUserName());
        });
        return articles;
    }

    @Override
    public List<Article> findArticlesByOptionalParameters(ArticleParam param) {
        //开启分页
        PageHelper.startPage(param == null ? 1 : param.getPage(), param == null ? 0 : param.getLimit());
        List<Article> articles = articleMapper.selectArticlesByOptionalParameters(param);
        //查找文章对应的用户信息，并填充到相关属性
        articles.stream().forEach(article -> {
            User user = userMapper.selectByPrimaryKey(article.getUserId());
            article.setUser(new HashMap<>());
            article.getUser().put("userId", user.getUserId());
            article.getUser().put("userName", user.getUserName());
        });
        return articles;
    }

    @Override
    public ArticleWithBLOBs findArticleByIdWithBLOBs(Integer id) {
        ArticleWithBLOBs articleWithBLOBs = articleMapper.selectByPrimaryKeyWithBLOBs(id);
        if (articleWithBLOBs == null) {
            throw new CustomizeException(ResultCode.NO_RESULT);
        }
        User user = userMapper.selectByPrimaryKey(articleWithBLOBs.getUserId());
        articleWithBLOBs.setUser(new HashMap<>());
        articleWithBLOBs.getUser().put("userId", user.getUserId());
        articleWithBLOBs.getUser().put("userName", user.getUserName());
        return articleWithBLOBs;
    }

    @Override
    public Article findArticleById(Integer id) {
        Article article = articleMapper.selectByPrimaryKey(id);
        if (article == null) {
            throw new CustomizeException(ResultCode.NO_RESULT);
        }
        User user = userMapper.selectByPrimaryKey(article.getUserId());
        article.setUser(new HashMap<>());
        article.getUser().put("userId", user.getUserId());
        article.getUser().put("userName", user.getUserName());
        return article;
    }

    @Override
    public int create(ArticleWithBLOBs record) {
        int result = articleMapper.insertSelective(record);
        //如果添加成功，接收回调的主键
        Integer articleId = record.getArticleId();
        List<Tag> tags = record.getTags();
        //添加标签和文章的对应关系
        for (Tag tag : tags) {
            if (tag.getTagId() != null) {
                articleTagsMapper.insert(record.getArticleId(), tag.getTagId());
            }
        }
        return result;
    }

    @Override
    public int deleteById(Integer id) {
        return articleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(ArticleWithBLOBs record) {
        record.setUpdateTime(new Date());
        //删除旧标签
        articleTagsMapper.deleteTagsByArticleId(record.getArticleId());
        //获取新标签
        List<Tag> tags = record.getTags();
        //添加新标签和文章的对应关系
        for (Tag tag : tags) {
            if (tag.getTagId() != null) {
                articleTagsMapper.insert(record.getArticleId(), tag.getTagId());
            }
        }
        return articleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return articleMapper.getCount();
    }

    /**
     * 没有实现，不使用
     *
     * @param id
     * @return
     */
    @Override
    public ArticleWithBLOBs retrieveById(Integer id) {
        return null;
    }

    /**
     * 没有实现，不使用
     *
     * @param page
     * @param limit
     * @return
     */
    @Override
    public List<ArticleWithBLOBs> retrieveAllByPaging(Integer page, Integer limit) {
        return null;
    }
}
