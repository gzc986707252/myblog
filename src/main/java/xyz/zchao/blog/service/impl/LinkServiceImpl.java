package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import xyz.zchao.blog.dao.LinkMapper;
import xyz.zchao.blog.pojo.Link;
import xyz.zchao.blog.service.LinkService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 1:26
 */
@Service
public class LinkServiceImpl implements LinkService {
    @Resource
    private LinkMapper linkMapper;

    @Override
    public int create(Link record) {
        return linkMapper.insertSelective(record);
    }

    @Override
    public Link retrieveById(Integer id) {
        return linkMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return linkMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Link record) {
        return linkMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return linkMapper.getCount();
    }

    @Override
    public List<Link> retrieveAllByPaging(Integer page, Integer limit) {
        return linkMapper.selectLinkByPaging(page, limit);
    }
}
