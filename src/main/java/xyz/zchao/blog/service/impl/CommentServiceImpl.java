package xyz.zchao.blog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.zchao.blog.dao.ArticleMapper;
import xyz.zchao.blog.dao.CommentMapper;
import xyz.zchao.blog.dao.UserMapper;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.Comment;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.service.CommentService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @Description: 评论留言业务实现类
 * @Author: guozongchao
 * @Date: 2020/5/10 12:46
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CommentServiceImpl implements CommentService {

    @Resource
    private CommentMapper commentMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ArticleMapper articleMapper;

    @Override
    public int create(Comment record) {
        return commentMapper.insertSelective(record);
    }

    @Override
    public Comment retrieveById(Integer id) {
        Comment comment = commentMapper.selectByPrimaryKey(id);
        packageCommentDetailData(comment);
        return comment;
    }

    @Override
    public int deleteById(Integer id) {
        return commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Comment record) {
        return commentMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return commentMapper.getCount();
    }

    @Override
    public List<Comment> retrieveAllByPaging(Integer page, Integer limit) {
        return null;
    }


    @Override
    public List<Comment> getParentComments(Integer articleId) {
        List<Comment> parentCommets=commentMapper.selectParentComments(articleId);
        parentCommets.stream().forEach(parent->{
            packageCommentDetailData(parent);
            findChildComments(parent);
        });
        return parentCommets;
    }


    /**
     * 递归每一个父评论的子评论，生成树结构
     * @param comment
     */
    private void findChildComments(Comment comment) {
        List<Comment> childComments = commentMapper.selectChildComments(comment.getArticleId(), comment.getCommentId());
        comment.setChildNode(childComments);
        if(childComments.size()==0){
            return;
        }
        childComments.stream().forEach(child->{
            packageCommentDetailData(child);
            findChildComments(child);
        });
    }

    /**
     * 将评论的发表用户、回复用户以及评论的文章等细节信息封装到Comment对象
     * @param comment
     */
    private void packageCommentDetailData(Comment comment) {

        //封装发表用户的信息
        User fromUser = userMapper.selectByPrimaryKey(comment.getUserId());
        comment.setFromUser(new HashMap<>());
        comment.getFromUser().put("userId",fromUser.getUserId());
        comment.getFromUser().put("userName", fromUser.getUserName());
        comment.getFromUser().put("avatar", fromUser.getAvatar());
        comment.getFromUser().put("role",fromUser.getRole());
        //封装回复用户信息
        if (comment.getParentId() != null) {
            //获取父评论
            Comment parentComment = commentMapper.selectByPrimaryKey(comment.getParentId());
            //获取父评论的发表者
            User toUser = userMapper.selectByPrimaryKey(parentComment.getUserId());
            comment.setToUser(new HashMap<>());
            comment.getToUser().put("userId", toUser.getUserId());
            comment.getToUser().put("userName", toUser.getUserName());
            comment.getToUser().put("avatar", toUser.getAvatar());
            comment.getToUser().put("role",toUser.getRole());
        }
        //封装评论对应的文章信息（如果articleId不为null的话）
        if (comment.getArticleId() != null) {
            Article article = articleMapper.selectByPrimaryKey(comment.getArticleId());
            comment.setArticle(new HashMap<>());
            comment.getArticle().put("articleId", article.getArticleId());
            comment.getArticle().put("title", article.getTitle());
        }
    }

}
