package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import xyz.zchao.blog.dao.CategoryMapper;
import xyz.zchao.blog.pojo.Category;
import xyz.zchao.blog.service.CategoryService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/16 16:25
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Resource
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> findCategorysByName(String keyword) {
        return categoryMapper.selectByCategoryName(keyword);
    }

    @Override
    public int create(Category record) {
        return categoryMapper.insertSelective(record);
    }

    @Override
    public Category retrieveById(Integer id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return categoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Category record) {
        return categoryMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return categoryMapper.getCount();
    }

    @Override
    public List<Category> retrieveAllByPaging(Integer page, Integer limit) {
        return categoryMapper.selectCategoriesByPaging(page, limit);
    }
}
