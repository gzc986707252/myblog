package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import xyz.zchao.blog.dao.NoticeMapper;
import xyz.zchao.blog.pojo.Notice;
import xyz.zchao.blog.service.NoticeService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 0:51
 */
@Service
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeMapper noticeMapper;

    @Override
    public List<Notice> findNoticeByKeyword(String keyword) {
        return noticeMapper.selectByContentKeyword(keyword);
    }

    @Override
    public int create(Notice record) {
        return noticeMapper.insertSelective(record);
    }

    @Override
    public Notice retrieveById(Integer id) {
        return noticeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return noticeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Notice record) {
        return noticeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return noticeMapper.getCount();
    }

    @Override
    public List<Notice> retrieveAllByPaging(Integer page, Integer limit) {
        return noticeMapper.selectNoticeByPaging(page, limit);
    }
}
