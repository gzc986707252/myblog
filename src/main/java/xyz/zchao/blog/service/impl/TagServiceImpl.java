package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import xyz.zchao.blog.dao.TagMapper;
import xyz.zchao.blog.pojo.Tag;
import xyz.zchao.blog.service.TagService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/16 16:26
 */
@Service
public class TagServiceImpl implements TagService {

    @Resource
    private TagMapper tagMapper;

    @Override
    public int create(Tag record) {
        return tagMapper.insertSelective(record);
    }

    @Override
    public Tag retrieveById(Integer id) {
        return tagMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return tagMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Tag record) {
        return tagMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return tagMapper.getCount();
    }

    @Override
    public List<Tag> retrieveAllByPaging(Integer page, Integer limit) {
        return tagMapper.selectTagsByPaging(page, limit);
    }

    @Override
    public List<Tag> findTagsByName(String keyword) {
        return tagMapper.selectByTagName(keyword);
    }
}
