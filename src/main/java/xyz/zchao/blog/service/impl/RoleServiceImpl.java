package xyz.zchao.blog.service.impl;

import org.springframework.stereotype.Service;
import xyz.zchao.blog.dao.RoleMapper;
import xyz.zchao.blog.pojo.Role;
import xyz.zchao.blog.service.RoleService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleMapper roleMapper;


    @Override
    public int create(Role record) {
        return roleMapper.insertSelective(record);
    }

    @Override
    public Role retrieveById(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Integer id) {
        return roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Role record) {
        return roleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int getCount() {
        return roleMapper.getCount();
    }

    @Override
    public List<Role> retrieveAllByPaging(Integer page, Integer limit) {
        return roleMapper.selectRoleByPaging(page, limit);
    }


}
