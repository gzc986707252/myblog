package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Category;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/16 16:21
 */
public interface CategoryService extends BaseService<Category> {

    List<Category> findCategorysByName(String keyword);
}
