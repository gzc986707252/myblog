package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Diary;

import java.util.List;

/**
 * @Description: 基本业务接口
 * @Author: guozongchao
 * @Date: 2020/5/7 15:29
 */
public interface BaseService<T> {
    /**
     * 增加一条记录
     * @param record
     * @return
     */
    int create(T record);

    /**
     * 根据id查询一条记录
     * @param id
     * @return
     */
    T retrieveById(Integer id);

    /**
     * 根据id删除一条记录
     * @param id
     * @return
     */
    int deleteById(Integer id);

    /**
     * 更新一条记录
     * @param record
     * @return
     */
    int update(T record);

    /**
     * 获取数据总数
     * @return
     */
    int getCount();

    /**
     * 基本分页查询
     * @param page
     * @param limit
     * @return
     */
    List<T> retrieveAllByPaging(Integer page,Integer limit);
}
