package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Link;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 1:24
 */
public interface LinkService extends BaseService<Link> {
}
