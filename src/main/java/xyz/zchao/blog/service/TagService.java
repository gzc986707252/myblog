package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Tag;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/16 16:25
 */
public interface TagService extends BaseService<Tag> {
    List<Tag> findTagsByName(String keyword);
}
