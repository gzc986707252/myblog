package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Role;

import java.util.List;

public interface RoleService extends BaseService<Role>{
}
