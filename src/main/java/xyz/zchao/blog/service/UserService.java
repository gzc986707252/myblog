package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.User;

import java.util.List;

public interface UserService extends BaseService<User> {

    User findUserByUserName(String userName);

    User findUserByRoleId(Integer roleId);

    List<User> fuzzyFindUserByUserName(String userName);
}
