package xyz.zchao.blog.service;

import xyz.zchao.blog.pojo.Notice;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 0:48
 */
public interface NoticeService extends BaseService<Notice> {
    List<Notice> findNoticeByKeyword(String keyword);
}
