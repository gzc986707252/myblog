package xyz.zchao.blog.parameter;

/**
 * @Description: 基础参数类
 * @Author: guozongchao
 * @Date: 2020/5/19 13:15
 */
public class BaseParam {
    private Integer id;
    //定义查询的页数
    private int page;
    //定义每页查询的数量
    private int limit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page == null ? 0 : page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit == null ? 0 : limit;
    }

    @Override
    public String toString() {
        return "BaseParam{" +
                "id=" + id +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }
}
