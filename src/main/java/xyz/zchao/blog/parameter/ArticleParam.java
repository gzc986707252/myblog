package xyz.zchao.blog.parameter;

/**
 * @Description: 文章搜索的参数类
 * @Author: guozongchao
 * @Date: 2020/5/19 13:13
 */
public class ArticleParam extends BaseParam {
    private String title;      //文章标题
    private Integer categoryId;  //文章分类ID
    private Boolean isOriginal;  //文章是否原创
    private Boolean isPublic;  //文章是否公开
    private Boolean isTop; //文章是否置顶
    private Boolean isRecommend;  //文章是否推荐
    private Boolean isPublish;   //文章是否发布

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getIsOriginal() {
        return isOriginal;
    }

    public void setIsOriginal(Boolean original) {
        isOriginal = original;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Boolean getIsTop() {
        return isTop;
    }

    public void setIsTop(Boolean top) {
        isTop = top;
    }

    public Boolean getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Boolean recommend) {
        isRecommend = recommend;
    }

    public Boolean getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Boolean publish) {
        isPublish = publish;
    }

    @Override
    public String toString() {
        return "ArticleParam{" +
                super.toString() +
                "title='" + title + '\'' +
                ", categoryId=" + categoryId +
                ", isOriginal=" + isOriginal +
                ", isPublic=" + isPublic +
                ", isTop=" + isTop +
                ", isRecommend=" + isRecommend +
                ", isPublish=" + isPublish +
                '}';
    }
}
