package xyz.zchao.blog.controller;

import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import xyz.zchao.blog.exception.CustomizeException;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.service.UserService;
import xyz.zchao.blog.util.ResultCode;
import xyz.zchao.blog.util.ResultVO;
import xyz.zchao.blog.util.TencentCosUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 上传控制器
 * @Author: guozongchao
 * @Date: 2020/5/15 21:41
 */

@RestController
@RequestMapping("/upload")
public class UploadController {

    private static final Logger log = LoggerFactory.getLogger(UploadController.class);

    @Autowired
    private TencentCosUtil cosUtil;
    @Autowired
    private UserService userService;

    /**
     * 头像上传
     * @param uid
     * @param userAvatar
     * @param request
     * @return
     */
    @PostMapping("/avatar")
    public ResultVO uploadAvatar(@RequestParam(required = true) Integer uid, MultipartFile userAvatar, HttpServletRequest request) {
        String url;
        try {
            String contentType = userAvatar.getContentType();
            InputStream inputStream = userAvatar.getInputStream();
            //执行上传到云COS
            cosUtil.uploadObjectToCOS("gzcblog", "images/avatar/" + uid, contentType, inputStream);
            //获取上传成功的公网访问地址
            // “ ?imageMogr2/interlace/0|imageMogr2/gravity/center/crop/100x100 ” 这是腾讯云的图像处理接口,居中裁剪图片100x100
            url= cosUtil.getObjectUrl("gzcblog","images/avatar/" + uid)+"?imageMogr2/interlace/0|imageMogr2/gravity/center/crop/100x100";
        } catch (RuntimeException | IOException e) {
            throw new CustomizeException(ResultCode.UPLOAD_ERROR);
        }finally{
            //关闭cos客户端
            cosUtil.closeCOSClient();
        }
        log.info("上传成功");
        //更新用户数据库
        User user=new User();
        user.setUserId(uid);
        user.setAvatar(url);
        userService.update(user);
        log.info("更新成功");
        //封装响应数据
        Map<String, Object> data = new HashMap<>();
        data.put("url",url);
        return new ResultVO(ResultCode.SUCCESS, data);
    }


    @PostMapping("/diary_img")
    public ResultVO uploadDiaryImg(MultipartFile diaryImage) {
        String src;
        try {
            //获取上传文件类型
            String contentType = diaryImage.getContentType();
            //获取上传文件输入流
            InputStream inputStream = diaryImage.getInputStream();
            //获取上传文件名
            String imageName=diaryImage.getOriginalFilename();
            //执行上传到云COS
            cosUtil.uploadObjectToCOS("gzcblog", "images/diary/" + imageName, contentType, inputStream);
            //获取上传成功的公网访问地址
            // “ ?imageMogr2/thumbnail/x60 ” 这是腾讯云的图像处理接口,指定目标图片高度为 60px，宽度等比压缩
            src= cosUtil.getObjectUrl("gzcblog","images/diary/" + imageName)+"?imageMogr2/thumbnail/x60";
        } catch (RuntimeException | IOException e) {
            e.printStackTrace();
            throw new CustomizeException(ResultCode.UPLOAD_ERROR);
        }finally{
            //关闭cos客户端
            cosUtil.closeCOSClient();
        }
        log.info("上传成功");
        //封装响应数据
        Map<String, Object> data = new HashMap<>();
        data.put("src",src);
        return new ResultVO(ResultCode.SUCCESS, data);
    }

}
