package xyz.zchao.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description: 后台管理菜单路由控制器
 * @Author: guozongchao
 * @Date: 2020/5/5 23:48
 */

@Controller
@RequestMapping("/menu")
public class MenuController {

    //编辑器
    @GetMapping("/b_md")
    public String toBlogEditor() {
        return "admin/page/blog-editor";
    }
    @GetMapping("/b_comment")
    public String toBlogComment() {
        return "admin/page/blog-comment";
    }
    @GetMapping("/w_comment")
    public String toWebsiteComment() {
        return "admin/page/website-comment";
    }
    @GetMapping("/b_list")
    public String toBlogList() {
        return "admin/page/blog-list";
    }
    @GetMapping("/category")
    public String toCategory() {
        return "admin/page/category";
    }
    @GetMapping("/daily")
    public String toDaily() {
        return "admin/page/daily";
    }
    @GetMapping("/links")
    public String toLinks() {
        return "admin/page/links";
    }
    @GetMapping("/notice")
    public String toNotice() {
        return "admin/page/notice";
    }
    @GetMapping("/role")
    public String toRole() {
        return "admin/page/role";
    }
    @GetMapping("/users")
    public String toUsers() {
        return "admin/page/users";
    }
    @GetMapping("/tags")
    public String toTags() {
        return "admin/page/tags";
    }
    @GetMapping("/welcome")
    public String toWelcome() {
        return "admin/page/welcome";
    }
    @GetMapping("/setting")
    public String toSetting() {
        return "admin/page/setting";
    }
    @GetMapping("/photo_album")
    public String toPhotoAlbum() {
        return "admin/page/photo-album";
    }

}
