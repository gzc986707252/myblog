package xyz.zchao.blog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.zchao.blog.annotation.IgnoreToken;
import xyz.zchao.blog.exception.CustomizeException;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.*;
import xyz.zchao.blog.service.*;
import xyz.zchao.blog.util.SummaryUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class RouterController {
    private static final Logger log = LoggerFactory.getLogger(RoleController.class);
    @Autowired
    private DiaryService diaryService;
    @Autowired
    private UserService userService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TagService tagService;
    @Autowired
    private ArticleService articleService;

    @GetMapping({"/", "/index"})
    public String toIndex(Model model) {
        try {
            //获得博主的信息
            User blogger = userService.findUserByRoleId(2);
            model.addAttribute("blogger", blogger);
        } catch (CustomizeException e) {
            log.error(e.getMessage());
            model.addAttribute("blogger", new User());
        }
        ArticleParam param = new ArticleParam();
        param.setIsRecommend(true);
        List<ArticleWithBLOBs> articles = articleService.findArticlesByOptionalParametersWithBLOBs(param);
        //获取文章的简短摘要
        articles.stream().forEach(article -> {
            article.setContentHtml(SummaryUtil.getSummary(article.getContentHtml()));
        });
        model.addAttribute("articles", articles);
        return "index";
    }

    /**
     * 前台文章专栏页面
     *
     * @param model
     * @return
     */
    @IgnoreToken
    @GetMapping("/article")
    public String toArticle(Model model) {

        return "article";
    }

    @GetMapping("/timeline")
    public String toDiaryTimeline(Model model) {
        return "timeline";
    }
    @GetMapping("/photo")
    public String toPhoto(){
        return "photo";
    }

    @GetMapping("/leavemessage")
    public String toLeavemessage() {
        return "leaveMessage";
    }

    @GetMapping("/login")
    public String toLogin() {
        return "login";
    }

    @GetMapping("/register")
    public String toRegister() {
        return "register";
    }
}
