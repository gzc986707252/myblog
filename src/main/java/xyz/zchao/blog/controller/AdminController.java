package xyz.zchao.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import xyz.zchao.blog.annotation.IgnoreToken;
import xyz.zchao.blog.parameter.BaseParam;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.util.ResultCode;
import xyz.zchao.blog.util.ResultVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class AdminController {
    /**
     * 路由到后台管理页面
     *
     * @return
     */
    @IgnoreToken
    @GetMapping("/index")
    public String toAdminIndex() {
        return "admin/index";
    }

    @GetMapping("test")
    @ResponseBody
    public void test(@RequestParam(required = false) Integer cid) {
        BaseParam param = new BaseParam();
        param.setPage(cid);
        System.out.println(param);
    }
}
