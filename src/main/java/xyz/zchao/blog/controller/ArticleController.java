package xyz.zchao.blog.controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import xyz.zchao.blog.annotation.IgnoreToken;
import xyz.zchao.blog.exception.CustomizeException;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;
import xyz.zchao.blog.service.ArticleService;
import xyz.zchao.blog.util.PageResultVO;
import xyz.zchao.blog.util.ResultCode;
import xyz.zchao.blog.util.ResultVO;
import xyz.zchao.blog.util.SummaryUtil;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: guozongchao
 * @Date: 2020/5/8 21:56
 */
@Controller
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @IgnoreToken
    @GetMapping("/details/{aid}")
    public String articleDetail(@PathVariable("aid") Integer aid, Model model) {
        ArticleWithBLOBs article = articleService.findArticleByIdWithBLOBs(aid);
        model.addAttribute("article", article);
        return "details";
    }

    /**
     * 获取博客列表
     *
     * @param param
     * @param type  0-获取文章不包括内容，1-获取文章包括内容
     * @return
     */
    @IgnoreToken
    @GetMapping("/list")
    @ResponseBody
    public PageResultVO getArticles(@RequestParam(required = true) Integer type, ArticleParam param) {

        if (type == 0) {
            List<Article> articles = articleService.findArticlesByOptionalParameters(param);
            PageInfo<Article> pageInfo = new PageInfo<>(articles);
            return new PageResultVO(ResultCode.SUCCESS, articles, pageInfo.getTotal());
        } else if (type == 1) {
            List<ArticleWithBLOBs> articleWithBLOBsList = articleService.findArticlesByOptionalParametersWithBLOBs(param);
            PageInfo<ArticleWithBLOBs> pageInfo = new PageInfo<>(articleWithBLOBsList);
            //获取文章的简短摘要
            articleWithBLOBsList.stream().forEach(article -> {
                article.setContentHtml(SummaryUtil.getSummary(article.getContentHtml()));
                article.setContentMd("");
            });
            return new PageResultVO(ResultCode.SUCCESS, articleWithBLOBsList, pageInfo.getTotal());
        } else {
            //type为其他类型则抛出参数校验异常
            throw new CustomizeException(ResultCode.VALIDATE_FAILED);
        }
    }

    /**
     * 获取单个文章
     *
     * @param aid
     * @param type 0-获取文章不包括内容，1-获取文章包括内容
     * @return
     */
    @IgnoreToken
    @GetMapping("/list/{aid}")
    @ResponseBody
    public ResultVO getArticle(@PathVariable("aid") Integer aid, @RequestParam(required = true) Integer type) {
        if (type == 0) {
            Article article = articleService.findArticleById(aid);
            return new ResultVO(ResultCode.SUCCESS, article);
        } else if (type == 1) {
            ArticleWithBLOBs articleWithBLOBs = articleService.findArticleByIdWithBLOBs(aid);
            return new ResultVO(ResultCode.SUCCESS, articleWithBLOBs);
        } else {
            throw new CustomizeException(ResultCode.VALIDATE_FAILED);
        }
    }

    /**
     * 更新文章
     *
     * @param aid
     * @param articleWithBLOBs
     * @return
     */
    @PutMapping("/list/{aid}")
    @ResponseBody
    public ResultVO updateArticle(@PathVariable("aid") Integer aid, @RequestBody @Valid ArticleWithBLOBs articleWithBLOBs) {
        articleWithBLOBs.setArticleId(aid);
        articleService.update(articleWithBLOBs);
        return new ResultVO(ResultCode.SUCCESS, null);
    }

    /**
     * 删除文章
     *
     * @param aid
     * @return
     */
    @DeleteMapping("/list/{aid}")
    @ResponseBody
    public ResultVO deleteArticle(@PathVariable("aid") Integer aid) {
        articleService.deleteById(aid);
        return new ResultVO(ResultCode.SUCCESS, null);
    }

    /**
     * 添加文章
     *
     * @param articleWithBLOBs
     * @return
     */
    @PostMapping("/list/add")
    @ResponseBody
    public ResultVO addArticle(@RequestBody @Valid ArticleWithBLOBs articleWithBLOBs) {
        articleWithBLOBs.setUserId(1);
        articleService.create(articleWithBLOBs);
        return new ResultVO(ResultCode.SUCCESS, null);
    }

    /**
     * 根据文章的标题搜索文章
     *
     * @param type  type 0-获取文章不包括内容，1-获取文章包括内容
     * @param param
     * @return
     */
    @IgnoreToken
    @GetMapping("/search")
    @ResponseBody
    public ResultVO searchArticles(@RequestParam(required = true) Integer type, ArticleParam param) {
        if (type == 0) {
            List<Article> articles = articleService.findArticlesByOptionalParameters(param);
            return new ResultVO(ResultCode.SUCCESS, articles);
        } else if (type == 1) {
            List<ArticleWithBLOBs> articleWithBLOBs = articleService.findArticlesByOptionalParametersWithBLOBs(param);
            return new ResultVO(ResultCode.SUCCESS, articleWithBLOBs);
        } else {
            throw new CustomizeException(ResultCode.VALIDATE_FAILED);
        }
    }

    @IgnoreToken
    @GetMapping("/category/{cid}")
    @ResponseBody
    public String getArticlesByCategoryId(@PathVariable Integer cid) {
        return "待完善";
    }

    @IgnoreToken
    @GetMapping("/tag/{tid}")
    @ResponseBody
    public String getArticlesByTagId(@PathVariable Integer tid) {
        return "待完善";
    }

}
