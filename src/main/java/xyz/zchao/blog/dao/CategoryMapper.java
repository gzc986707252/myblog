package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import xyz.zchao.blog.pojo.Category;

import java.util.List;

public interface CategoryMapper {
    int deleteByPrimaryKey(Integer categoryId);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer categoryId);

    List<Category> selectByCategoryName(@Param("keyword") String keyword);


    int updateByPrimaryKeySelective(Category record);

    int getCount();

    int updateByPrimaryKey(Category record);

    List<Category> selectCategoriesByPaging(@Param("page") Integer page, @Param("limit") Integer limit);

}