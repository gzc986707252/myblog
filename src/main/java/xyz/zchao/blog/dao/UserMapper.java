package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.User;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectByUserName(String userName);

    User selectByUserRoleId(Integer roleId);

    int getCount();

    List<User> fuzzyQueryUserByUserName(@Param("userName") String userName);

    List<User> selectUsersByPaging(@Param("page") Integer page, @Param("limit") Integer limit);
}