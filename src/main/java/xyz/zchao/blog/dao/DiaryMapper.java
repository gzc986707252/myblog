package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.Diary;

import java.util.List;

public interface DiaryMapper {
    int deleteByPrimaryKey(Integer diaryId);

    int insert(Diary record);

    int insertSelective(Diary record);

    Diary selectByPrimaryKey(Integer diaryId);

    int updateByPrimaryKeySelective(Diary record);

    int updateByPrimaryKey(Diary record);

    int getCount();

    List<Diary> selectAllByPaging(@Param("page") Integer page, @Param("limit") Integer limit);
}