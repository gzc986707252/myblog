package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.Role;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleId);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleId);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> selectRoleByPaging(@Param("page") Integer page, @Param("limit") Integer limit);

    int getCount();
}