package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.Category;
import xyz.zchao.blog.pojo.Tag;

import java.util.List;

public interface TagMapper {
    int deleteByPrimaryKey(Integer tagId);

    int insert(Tag record);

    int insertSelective(Tag record);

    Tag selectByPrimaryKey(Integer tagId);

    int updateByPrimaryKeySelective(Tag record);

    int updateByPrimaryKey(Tag record);

    List<Tag> selectTagsByPaging(@Param("page") Integer page, @Param("limit") Integer limit);

    List<Tag> selectByTagName(@Param("keyword") String keyword);

    int getCount();
}