package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.annotation.IgnoreToken;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;

import java.util.List;
import java.util.Map;

public interface ArticleMapper {
    int deleteByPrimaryKey(Integer articleId);

    int insert(ArticleWithBLOBs record);

    int insertSelective(ArticleWithBLOBs record);

    Article selectByPrimaryKey(Integer articleId);

    ArticleWithBLOBs selectByPrimaryKeyWithBLOBs(Integer articleId);


    int updateByPrimaryKeySelective(ArticleWithBLOBs record);


    int updateByPrimaryKey(Article record);

    int getCount();

    List<ArticleWithBLOBs> selectArticlesByOptionalParametersWithBLOBs(ArticleParam param);

    List<Article> selectArticlesByOptionalParameters(ArticleParam param);

}