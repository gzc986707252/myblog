package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.Comment;

import java.util.List;

public interface CommentMapper {
    int deleteByPrimaryKey(Integer commentId);

    int insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer commentId);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    int getCount();

    /**
     * 查询父评论
     * 参数articleId是文章id，当不为null时，查询该文章的所有父评论，否则查询网站留言的父评论
     * @param articleId
     * @return
     */
    List<Comment> selectParentComments(@Param("articleId") Integer articleId);

    /**
     * 查询子评论
     * 参数articleId是文章id，当不为null时，查询该文章的某父评论的所有子评论，否则查询网站留言某父评论的子评论
     * parentId 是父评论ID 必须要有
     * @param articleId
     * @param parentId
     * @return
     */
    List<Comment> selectChildComments(@Param("articleId") Integer articleId,@Param("parentId") Integer parentId);
}