package xyz.zchao.blog.dao;

import org.apache.ibatis.annotations.Param;
import xyz.zchao.blog.pojo.Tag;

import java.util.List;

/**
 * @Description: 文章和标签的多对多关系
 * @Author: guozongchao
 * @Date: 2020/5/17 18:59
 */
public interface ArticleTagsMapper {
    int insert(@Param("articleId") Integer articleId, @Param("tagId") Integer tagId);

    List<Tag> selectTagsByArticleId(@Param("articleId") Integer articleId);

    int deleteTagsByArticleId(@Param("articleId") Integer articleId);

    int updateTagsByArticleId(@Param("articleId") Integer articleId, @Param("oldTagId") Integer oldTagId, @Param("newTagId") Integer newTagId);
}
