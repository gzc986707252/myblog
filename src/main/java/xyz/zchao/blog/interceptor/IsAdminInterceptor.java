package xyz.zchao.blog.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.servlet.HandlerInterceptor;
import xyz.zchao.blog.pojo.User;
import xyz.zchao.blog.util.ResultCode;
import xyz.zchao.blog.util.ResultVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 管理员权限拦截器
 */
public class IsAdminInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.setContentType("application/json");
        HttpSession session=request.getSession();
        User user =(User) session.getAttribute("user");
        ObjectMapper objectMapper = new ObjectMapper();
        if(user.getRoleId()==1){
            response.getWriter().write(objectMapper.writeValueAsString(new ResultVO<String>(ResultCode.ACCESS_DENIED, "抱歉,你没有权限访问!")));
            return false;
        }
        return true;
    }
}
