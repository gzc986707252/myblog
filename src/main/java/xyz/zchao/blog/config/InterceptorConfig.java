package xyz.zchao.blog.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.zchao.blog.interceptor.IsAdminInterceptor;
import xyz.zchao.blog.interceptor.TokenValidationInterceptor;

/**
 * 拦截器的配置类
 */
@Configuration   //表示该配置类被Spring容器创建
public class InterceptorConfig implements WebMvcConfigurer {
    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new TokenValidationInterceptor())
                .addPathPatterns("/user/**")
                .addPathPatterns("/comment/**")
                .addPathPatterns("/diary/**")
                .addPathPatterns("/link/**")
                .addPathPatterns("/tag/**")
                .addPathPatterns("/category/**")
                .addPathPatterns("/role/**")
                .addPathPatterns("/notice/**")
                .addPathPatterns("/upload/**")
                .addPathPatterns("/article/**");


    /*    registry.addInterceptor(new IsAdminInterceptor())
                .addPathPatterns("/admin/**")
                .addPathPatterns("/menu/*");*/
    }
}
