layui.use(['element', 'jquery','layer','form'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		layer = layui.layer;
	
	 //监听提交
	form.on('submit(btn-login)',function(data){
		 $.ajax({
			url:'/user/login',
			type:'post',
			data:JSON.stringify(data.field),
			contentType:'application/json',
			 dataType:'json',
			success:function (res) {
				if (res.code==0){
					layer.msg("登录成功", {
						icon: 1,
						anim: 1,
						time: 1500 //1.5秒关闭（如果不配置，默认是3秒）
					}, function(){
						window.location.href="/";
					});
				}else {
					layer.msg(res.data, {
						icon: 2,
						anim: 6,
						time: 1500
					});
				}
			}
		 });
		//关闭表单跳转		
		return false;
	});
	
	//自定义验证规则
	  form.verify({
	    username: function(value){
	      if(value.length < 2||value.length>10){
	        return '用户名必须3~10个字符';
	      }
	    }
	    ,password: [
	      /^[\S]{6,12}$/
	      ,'密码必须6到12位，且不能出现空格'
	  	]

	  });
	
});