layui.use(['element', 'jquery', 'layer', 'form', 'upload'], function () {
    var element = layui.element,
        $ = layui.jquery,
        laypage = layui.laypage,
        util = layui.util,
        form = layui.form,
        upload = layui.upload,
        layer = layui.layer;


    var cancel = false;
    $("#modify-btn").click(function () {
        if (cancel) {
            $("input[name='userName']").attr("readonly", "readonly");
            $("input[name='email']").attr("readonly", "readonly");
            $("textarea[name='signature']").attr("readonly", "readonly");
            $("#update-btn").addClass("layui-btn-disabled").attr("disabled", "disabled");
            $("#modify-btn").removeClass("layui-btn-primary").text("修改");
            cancel = false;
        } else {
            $("input[name='userName']").removeAttr("readonly");
            $("input[name='email']").removeAttr("readonly");
            $("textarea[name='signature']").removeAttr("readonly");
            $("#update-btn").removeClass("layui-btn-disabled").removeAttr("disabled");
            $("#modify-btn").addClass("layui-btn-primary").text("取消");
            cancel = true;
        }
    });

    //监听提交
    form.on('submit(update-btn)', function (data) {
        var lastIndexOf = window.location.pathname.lastIndexOf("/");
        var uid = window.location.pathname.substring(lastIndexOf+1);
        $.ajax({
            url: '/user/' + uid,
            type: 'put',
            headers:{
                'Authorization': $.cookie("access_token")
            },
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data.field),
            success: function (res) {
                if(res.code==0){
                    layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
                        window.location.href = res.data;
                    });
                }else{
                    layer.msg(res.msg, {icon: 2, anim: 6, time: 1500});
                }
            }
        });
        //关闭表单跳转
        return false;
    });

    form.verify({
        username: function (value) {
            if (value.length < 2 || value.length > 10) {
                return '用户名必须3~10个字符';
            }
        }
    });


    //头像上传
    var uploadInst = upload.render({
        elem: '#uploadAvatar'
        , url: '/upload/avatar' //改成您自己的上传接口
        , data:{
            uid: $.cookie("uid")
        }
        ,headers: {
            'Authorization':$.cookie("access_token")
        }
        , accept:'images'
        , acceptMime:'image/*'
        , field:'userAvatar'  //后端接收的字段名
        , size: 2048  //限制图片大小为2M
        , auto:true  //开启自动上传
        // , bindAction:'#uploadAvatar'  //如果不是自动上传，则需要绑定一个开始上传按钮
        , before: function (obj) {    //执行提交前的回调
            //预读本地文件示例，不支持ie8
            obj.preview(function (index, file, result) {
                $('#avatar').attr('src', result); //图片链接（base64）
            });
        }
        , done: function (res) {   //响应成功的回调
            console.log(res);
            //如果上传失败
            if (res.code != 0) {
                return layer.msg(res.msg,{icon:2,anim:6,time:1500});
            }
            //上传成功
            return layer.msg(res.msg,{icon:1,anim:1,time:1500},function () {
                $('#avatar').attr('src', res.data.url);
            });

        }
        , error: function () {   //响应错误的回调
            //失败状态，并实现重传
            var demoText = $('#upload-msg');
            demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs upload-reload">重试</a>');
            demoText.find('.upload-reload').on('click', function () {
                uploadInst.upload();
            });
        }
    });
});