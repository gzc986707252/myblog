layui.use(['element', 'jquery', 'layer', 'form', 'table', 'layedit'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		table = layui.table,
		layedit = layui.layedit,
		layer = layui.layer;

	//渲染表格数据
	var dataTb=table.render({
		elem: '#dataTable',
		url: '/diary/list',
		headers: {
			'Authorization': $.cookie("access_token")
		},
		toolbar: '#tableToolbar', //开启头部工具栏，并为其绑定左侧模板
		height: 550,
		cols: [
			[{
				type: 'checkbox',
				fixed: 'left'
			}, {
				field: 'diaryId',
				title: '日记ID',
				width: 120,
					fixed: 'left',
					unresize: true,
					sort: true
				}, {
					field: 'content',
					title: '日记内容',
					width: 400,
				}, {
					field: 'userName',
					title: '发布用户',
					width: 120,
					templet:function (data) {
						return data.user.userName;
					}
				},
				{
					field: 'createTime',
					title: '发布时间',
					width: 200,
				},
				{
					field: 'updateTime',
					title: '更新时间',
					width: 200,
				},
				{
					fixed: 'right',
					title: '操作',
					toolbar: '#tableBar',
					width: 150
				}
			]
		],
		page: true
	});

	//头工具栏事件
	table.on('toolbar(dataTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
			case 'deleteSelected':
				var data = checkStatus.data;
				layer.alert(JSON.stringify(data));
				break;
		};
	});

	//监听行工具事件
	table.on('tool(dataTable)', function(obj) {
        var data = obj.data;
        // console.log(obj)
        if (obj.event === 'del') {
            layer.confirm('真的删除该数据么', function (index) {
                $.ajax({
                    url: '/diary/' + data.diaryId,
                    type: 'Delete',
                    headers: {
                        "Authorization": $.cookie("access_token")
                    },
                    dataType: 'json',
                    success: function (res) {
						if(res.code==0){
							layer.msg(res.msg,{icon:1,anim:1,time:1500});
							obj.del();
						}else {
							layer.msg(res.msg,{icon:2,anim:6,time:1500});
						}
					},
					error:function () {
						layer.msg("服务端错误啦", { icon: 2,anim: 6});
					}
				});
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			layeditorInit("编辑内容","/diary/update", {'diaryId':data.diaryId,'content':data.content});
		}
	});

	/**
	 * 发布日记
	 */
	$("#btn_add").click(function() {
		layeditorInit("发布新日记","/diary/add",{'content':null} );
	});

	/**
	 * 搜索
	 */
	$("#btn_search").click(function() {
		layer.prompt({
			formType: 0,
			title: "请输入搜索关键字",
			btn: "搜索",
		}, function(value, index) {
			/* ajax 请求搜索 */
			layer.msg(value);
			layer.close(index);
		});
	});

	/**
	 * 初始化编辑器,同时处理添加和更新的请求发送
	 * @param title 弹窗的标题
	 * @param post_url 请求的url
	 * @param data  请求的数据，可为null
	 */
	function layeditorInit(title, post_url,data) {
		var reply_idex;
		layer.open({
			type: 1,
			content: '<div style="margin:10px"><textarea class="layui-textarea" id="daily-editor" style="display: none;"></textarea></div>',
			title: title,
			btn: '提交',
			anim: 1,
			area: ['550px', '320px'],
			maxmin: true,
			yes: function(index) {
				//获取编辑器的值（HTML源码）
				var newValue = layedit.getContent(reply_idex);
				if(newValue==''||newValue.length==0)
					return;
				data.content=newValue;
				/**
				 * ajax请求添加或更新处理
				 */
				$.ajax({
					url:post_url,
					type:'post',
					headers:{
						"Authorization": $.cookie("access_token")
					},
					data:JSON.stringify(data),
					contentType:'application/json',
					dataType:'json',
					success:function (res) {
						if(res.code==0){
							layer.msg(res.msg, {
								icon: 1,
								anim: 1,
								time:1500
							},function () {
								//重载表格
								dataTb.reload();
							});
						}else{
							layer.msg(res.msg, {
								icon: 2,
								anim: 6
							});
						}
					},
					error:function () {
						return layer.msg("服务端错误啦", {
							icon: 2,
							anim: 6
						});
					}
				});
				layer.close(index);
			},
			success: function() {
				//初始化编辑器
				reply_idex = layedit.build('daily-editor', {
					height: 150
				});
				if (data != null) {
					//获取子iframe的页面元素
					// console.log($("#LAY_layedit_"+reply_idex).contents().find('body'));
					//填充编辑器
					$("#LAY_layedit_"+reply_idex).contents().find('body').html(data.content);
				}
			}
		});
	}
});
