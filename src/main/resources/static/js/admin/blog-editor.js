layui.use(['element', 'jquery', 'layer', 'form'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		layer = layui.layer;

	//执行填充编辑器操作，主要用于文章的编辑
	fillUpEditor();

	//从服务端获取分类数据,并存放到sessionStorage
	$.getJSON("/category/list", function (res) {
		if (res.code != 0) {
			return;
		}
		layui.sessionData('blogCache', {
			key: 'category',
			value: res.data
		});
	});
	//从服务端中获取标签,并存放到sessionStorage
	$.getJSON("/tag/list", function (res) {
		if (res.code != 0) {
			return;
		}
		layui.sessionData('blogCache', {
			key: 'tag',
			value: res.data
		});
	});


	/**
	 * 监听博客类型下拉列表框,当选择转载类型时，显示转载url文本框
	 */
	form.on('select(blogType)', function (data) {
		// console.log(data.value); 得到被选中的值
		var value = data.value;
		if (value === "0") {
			$("#reprintUrl").show();
		} else {
			$("#reprintUrl").hide();
			$("#reprintUrl").val("");
		}
	});

	/**
	 * 清空按钮
	 */
	$("#btn-clear").click(function () {
		layer.confirm("清空后当前编辑文章的缓存就清除了哦?", {title: 'Are you sure？'}, function () {
			layui.sessionData("blogCache", null);
			window.location.reload();
		});
	});

	/**
	 * 监听博客发布按钮
	 */
	$("#btn-send").click(function () {
		//获取编辑器的内容
		let content = window.mdEditor.getHTML();
		let title = $("#title").val();
		if (content.length == 0 || title.length == 0) {
			layer.msg("标题或正文不能为空哦！", {
				icon: 5,
				anim: 6
			});
			return;
		}
		//从sessionStorage拿要编辑的文章数据,如果不存在就赋值{}
		let draftData = layui.sessionData("blogCache").article;
		let data = draftData == null ? {} : draftData;
		layer.open({
			type: 1,
			content: $("#blogPublishPopup").html(),
			title: "发布文章",
			btn: ['直接发布', '保存草稿'],
			anim: 1,
			area: ['690px', '400px'],
			maxmin: true,
			//直接发布按钮
			yes: function () {
				if (!getData(data))
					return;
				//设置发布字段为true
				data["isPublish"] = true;
				//判断articleId字段是否存在，存在则直接更新操作，不存在则添加操作
				if (data.articleId == undefined) {
					articleAddOrUpdate('add', data);
				} else {
					articleAddOrUpdate('update', data);
				}
			},
			//保存按钮
			btn2: function() {
				if (!getData(data))
					return;
				//设置发布字段为false
				data["isPublish"] = false;
				//判断articleId字段是否存在，存在则直接更新操作，不存在则添加操作
				if (data.articleId == undefined) {
					articleAddOrUpdate('add', data);
				} else {
					articleAddOrUpdate('update', data);
				}
			},
			//弹窗弹出后执行的回调函数，可以用来初始化一些控件元素
			success: function() {
				//从本地存储获取分类、标签缓存数据
				let category = layui.sessionData("blogCache").category;
				let tag = layui.sessionData("blogCache").tag;
				//遍历分类数据,并在对应的下拉列表框追加内容
				layui.each(category, function (index, item) {
					$("#category").append('<option value="' + item.categoryId + '">' + item.categoryName + '</option>');
				});
				//遍历标签数据,并在对应的下拉列表框追加内容
				layui.each(tag, function (index, item) {
					$("#tag1").append('<option value="' + item.tagId + '">' + item.tagName + '</option>');
					$("#tag2").append('<option value="' + item.tagId + '">' + item.tagName + '</option>');
					$("#tag3").append('<option value="' + item.tagId + '">' + item.tagName + '</option>');
				});
				//重新渲染弹窗的表单控件
				form.render();
				//当文章为编辑状态时(会话缓存中存在文章的数据)，把该文章的相关数据填充到对应控件
				let article = layui.sessionData("blogCache").article;
				if (article != null) {
					$("#category").val(article.category.categoryId);

					for (let i = 0; i < article.tags.length; i++) {
						$("#tag" + (i + 1)).val(article.tags[i].tagId);
					}

					$("#isOriginal").val(article.isOriginal ? '1' : '0');
					if (!article.isOriginal) {
						$("#reprintUrl").show();
						$("#reprintUrl").val(article.reprintUrl);
					}
					$("#isPublic").attr("checked", article.isPublic);
					$("#isTop").attr('checked', article.isTop);
					$("#isRecommend").attr("checked", article.isRecommend);
					//重新渲染弹窗的表单控件
					form.render();
				}
				//重新渲染弹窗的表单控件
				form.render();
			}
		});
	});


	/**
	 * 获取数据，传入{}无字段对象时，获取数据填充，传入对象有字段时，更新相应字段值
	 * @param {Object} data  该参数应该为一个对象{}
	 */
	function getData(data) {
		data["title"] = $("#title").val();
		data["contentHtml"] = window.mdEditor.getHTML(); // 获取 Textarea 保存的 HTML 源码
		data["contentMd"] = window.mdEditor.getMarkdown(); // 获取 Markdown 源码
		//获取分类
		let categoryId = $("#category").val();
		if (categoryId == 0) {
			layer.msg("必须要选择一个分类哦！", {
				icon: 0,
				anim: 6
			});
			return false;
		}
		data["categoryId"] = categoryId;
		//获取标签,放入标签数组
		data.tags = [];
		for (let i = 1; i <= 3; i++) {
			let tag = {};
			tag["tagId"] = $("#tag" + i).val();
			data.tags.push(tag);
		}

		//获取博客类型
		let isOriginal = $("#isOriginal").val();
		if (isOriginal == -1) {
			layer.msg("博客类型还是要滴！", {
				icon: 0,
				anim: 6
			});
			return false;
		}
		//获取发布类型开关值
		data["isPublic"] = $("#isPublic")[0].checked;
		//获取置顶开关值
		data["isTop"] = $("#isTop")[0].checked;
		//获取推荐开关值
		data["isRecommend"] = $("#isRecommend")[0].checked;
		//获取是否原创选项值
		data["isOriginal"] = isOriginal == 1 ? true : false;
		//获取转载url
		data["reprintUrl"] = $("#reprintUrl").val();
		return true;
	}

	/**
	 * 暂时保存博客数据到SessionStorage存储
	 * @param {Object} data
	 */
	function saveArticleToSessionStorage(data) {
		layui.sessionData("blogCache", {
			key: 'article',
			value: data
		});
	}

	/**
	 * 填充博客发布页面，主要用于读取保存草稿箱的博客到编辑器(如果有的话)
	 */
	function fillUpEditor() {
		//读取浏览器本地会话存储
		let article = layui.sessionData("blogCache").article;
		if (article == null)
			return;
		$("#title").val(article.title);
		$("#content-editormd-markdown-doc").val(article.contentMd);
	}

	/**
	 * 添加文章和更新文章都通过这里请求到后台服务器
	 * @param type
	 * @param data
	 */
	function articleAddOrUpdate(type, data) {
		if (type == 'add') {
			$.ajax({
				url: "/article/list/add",
				type: 'post',
				headers: {
					'Authorization': $.cookie('access_token')
				},
				data: JSON.stringify(data),
				contentType: 'application/json',
				dataType: 'json',
				success: function (res) {
					if (res.code != 0) {
						return layer.msg(res.msg, {icon: 2, anim: 6});
					}
					return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
						//清空本地缓存
						layui.sessionData("blogCache", {
							key: 'article',
							remove: true
						});
						//关闭弹窗
						layer.close();
						window.location.reload();
					});
				},
				error: function (xhr) {
					return layer.msg("服务器端错误：" + xhr.status, {icon: 2, anim: 6});
				}
			});
		} else if (type == "update") {
			$.ajax({
				url: "/article/list/" + data.articleId,
				type: 'put',
				headers: {
					'Authorization': $.cookie('access_token')
				},
				data: JSON.stringify(data),
				contentType: 'application/json',
				dataType: 'json',
				success: function (res) {
					if (res.code != 0) {
						return layer.msg(res.msg, {icon: 2, anim: 6});
					}
					return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
						//清空本地缓存
						layui.sessionData("blogCache", {
							key: 'article',
							remove: true
						});
						layer.close();
						window.location.reload();
					});
				},
				error: function (xhr) {
					return layer.msg("服务器端错误：" + xhr.status, {icon: 2, anim: 6});
				}
			});
		}
	}


});
