layui.use(['element', 'jquery', 'layer', 'form', 'table'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		table = layui.table,
		layer = layui.layer;

	//渲染分类表格数据
	var category_tb = table.render({
		elem: '#categoryTable',
		url: '/category/list',
		headers: {
			'Authorization': $.cookie("access_token")
		},
		toolbar: '#categoryTableToolbar', //开启头部工具栏，并为其绑定左侧模板
		height: 550,
		cols: [
			[{
				type: 'checkbox',
				fixed: 'left'
			}, {
				field: 'categoryId',
				title: '分类ID',
				width: 200,
				fixed: 'left',
				unresize: true,
				sort: true
			}, {
				field: 'categoryName',
				title: '分类名',
				width: 400,
			}, {
				fixed: 'right',
				title: '操作',
				toolbar: '#categoryTableBar',
				width: 150
			}]
		],
		page: true
	});

	//头工具栏事件
	table.on('toolbar(categoryTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
			case 'deleteSelected':
				var data = checkStatus.data;
				//待完善
				layer.alert(JSON.stringify(data));
				break;
		};
	});

	//监听行工具事件
	table.on('tool(categoryTable)', function(obj) {
		var data = obj.data;
		//console.log(obj)
		if (obj.event === 'del') {
			layer.confirm('真的删除行么', function(index) {
				$.ajax({
					url: '/category/list/' + data.categoryId,
					type: 'delete',
					headers: {
						'Authorization': $.cookie("access_token")
					},
					dataType: 'json',
					success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.del();
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			layer.prompt({
				formType: 0,
				title:"编辑",
				value: data.categoryName
			}, function(value, index) {
				//执行ajax请求更新
				$.ajax({
					url: '/category/list/' + data.categoryId,
					type: 'put',
					headers: {
						'Authorization': $.cookie("access_token")
					},
					data: JSON.stringify({'categoryName': value}),
					contentType: 'application/json',
					dataType: 'json',
					success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.update({
								categoryName: value
							});
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		}
	});
	
	/**
	 * 添加分类
	 */
	$("#btn_category_add").click(function(){
		layer.prompt({
			formType: 0,
			title:"请输入要添加的分类名称",
			btn:"添加",
		}, function(value, index) {
			/* ajax 请求添加分类 */
			$.ajax({
				url: '/category/add',
				type: 'post',
				headers: {
					'Authorization': $.cookie("access_token")
				},
				data: JSON.stringify({'categoryName': value}),
				contentType: 'application/json',
				dataType: 'json',
				success: function (res) {
					if (res.code != 0) {
                        return layer.msg(res.msg, {icon: 2, anim: 6});
                    }
					return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
						category_tb.reload();
					});
				},
				error: function () {
					return layer.msg("服务器出错啦", {icon: 5, anim: 6})
				}
			});
			layer.close(index);
		});
	});

	/**
	 * 搜索分类
	 */
	$("#searchCategories").change(function () {
		var keyword = $("#searchCategories").val();
		//搜索到数据重新渲染表格
		category_tb.reload({
			url: '/category/search',
			where: {
				'keyword': keyword
			},
			page: {
				curr: 1, //从第一页开始
			}
		});
	});

});
