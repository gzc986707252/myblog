layui.use(['element', 'jquery', 'layer', 'form', 'table'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		table = layui.table,
		layer = layui.layer;

	//渲染表格数据
	var link_tb = table.render({
		elem: '#dataTable',
		url: '/link/list',
		headers: {
			'Authorization': $.cookie("access_token")
		},
		toolbar: '#tableToolbar', //开启头部工具栏，并为其绑定左侧模板
		height: 550,
		cols: [
			[{
				type: 'checkbox',
				fixed: 'left'
			}, {
				field: 'linkId',
				title: 'ID',
				width: 120,
				fixed: 'left',
				unresize: true,
				sort: true
			}, {
				field: 'linkName',
				title: '链接名称',
				width: 250,
			}, {
				field: 'url',
				title: 'URL',
				width: 400,
			}, {
				fixed: 'right',
				title: '操作',
				toolbar: '#tableBar',
				width: 150
			}]
		],
		page: true
	});

	//头工具栏事件
	table.on('toolbar(dataTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
			case 'deleteSelected':   //删除选中
				var data = checkStatus.data;
				layer.alert(JSON.stringify(data));
				break;
			case 'add':  //添加
				addOrEdit("add", null, null);
				break;
		};
	});

	//监听行工具事件
	table.on('tool(dataTable)', function(obj) {
        var data = obj.data;
        // console.log(obj)
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                $.ajax({
                    url: '/link/list/' + data.linkId,
                    type: 'delete',
                    headers: {
                        'Authorization': $.cookie("access_token")
                    },
                    dataType: 'json',
                    success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.del();
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			addOrEdit("edit", data, obj);
		}
	});


	/**
	 * 添加或编辑
	 * @param {Object} event 操作事件
	 * @param {Object} data  数据对象{}
	 * @param {Object} obj  表行对象{}
	 */
	function addOrEdit(event, data, obj) {
		layer.open({
			type: 1,
			content: '<div style="margin:10px">'
				+ '链接名称<input type="text" id="linkName" placeholder="请输入链接名称" autocomplete="off" class="layui-input">'
				+ '<br>URL<input type="text" id="url" placeholder="请输入URL" autocomplete="off" class="layui-input">'
				+ '</div>',
			title: event === 'add' ? '添加' : '编辑',
			btn: '提交',
			anim: 1,
			area: ['350px', '250px'],
			yes: function(index) {
				var linkName=$("#linkName").val();
				var url=$("#url").val();
				if(linkName.length==0||url.length==0)
					return ;
				var link= {
					"linkName": linkName,
					"url": url
				};
				if(event==="add") {
					//添加
					// layer.alert(JSON.stringify(link));
					$.ajax({
						url: '/link/add',
						type: 'post',
						headers: {
							'Authorization': $.cookie("access_token")
						},
						data: JSON.stringify(link),
						contentType: 'application/json',
						dataType: 'json',
						success: function (res) {
							if (res.code != 0) {
                                return layer.msg(res.msg, {icon: 2, anim: 6});
                            }
							return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
								link_tb.reload();
							});
						},
						error: function () {
							return layer.msg("服务器出错啦", {icon: 5, anim: 6})
						}
					});

				}
				else {
					//编辑
					// layer.alert(JSON.stringify(link));
					$.ajax({
						url: '/link/list/' + data.linkId,
						type: 'put',
						headers: {
							'Authorization': $.cookie("access_token")
						},
						data: JSON.stringify(link),
						contentType: 'application/json',
						dataType: 'json',
						success: function (res) {
							if (res.code != 0) {
                                return layer.msg(res.msg, {icon: 2, anim: 6});
                            }
							return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
								obj.update(link);
							});
						},
						error: function () {
							return layer.msg("服务器出错啦", {icon: 5, anim: 6})
						}
					});
				}
				
				layer.close(index);
			},
			success:function(){
				if(data!=null){
					$("#linkName").val(data.linkName);
					$("#url").val(data.url);
				}
			}
		});
	}

});
