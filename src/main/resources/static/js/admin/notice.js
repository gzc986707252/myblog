layui.use(['element', 'jquery', 'layer', 'form', 'table'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		table = layui.table,
		layer = layui.layer;

	//渲染表格数据
	var notice_tb = table.render({
		elem: '#dataTable',
		url: '/notice/list',
		headers: {
			'Authorization': $.cookie("access_token")
		},
		toolbar: '#tableToolbar', //开启头部工具栏，并为其绑定左侧模板
		height: 550,
		cols: [
			[{
				type: 'checkbox',
				fixed: 'left'
			}, {
				field: 'noticeId',
				title: '公告ID',
				width: 120,
				fixed: 'left',
				unresize: true,
				sort: true
			}, {
				field: 'content',
				title: '公告内容',
				width: 600,
			}, {
				field: 'createTime',
				title: '发布时间',
				width: 200,
				edit: 'text'
			},
			{
				fixed: 'right',
				title: '操作',
				toolbar: '#tableBar',
				width: 150
			}]
		],
		page: true
	});

	//头工具栏事件
	table.on('toolbar(dataTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
			case 'deleteSelected':
				var data = checkStatus.data;
				layer.alert(JSON.stringify(data));
				break;
		};
	});

	//监听行工具事件
	table.on('tool(dataTable)', function(obj) {
		var data = obj.data;
		//console.log(obj)
		if (obj.event === 'del') {
			layer.confirm('真的删除行么', function(index) {
				$.ajax({
					url: '/notice/list/' + data.noticeId,
					type: 'delete',
					headers: {
						'Authorization': $.cookie("access_token")
					},
					dataType: 'json',
					success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.del();
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			layer.prompt({
				formType: 2,
				title: "编辑",
				value: data.content
			}, function(value, index) {
				$.ajax({
					url: '/notice/list/' + data.noticeId,
					type: 'put',
					headers: {
						'Authorization': $.cookie("access_token")
					},
					data: JSON.stringify({'content': value}),
					contentType: 'application/json',
					dataType: 'json',
					success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.update({
								content: value
							});
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		}
	});
	
	/**
	 * 添加
	 */
	$("#btn_add").click(function(){
		layer.prompt({
			formType: 2,
			title:"请输入要发布的内容",
			btn:"发布",
		}, function(value, index) {
			/* ajax  */
			$.ajax({
				url: '/notice/add',
				type: 'post',
				headers: {
					'Authorization': $.cookie("access_token")
				},
				data: JSON.stringify({'content': value}),
				contentType: 'application/json',
				dataType: 'json',
				success: function (res) {
					if (res.code != 0) {
                        return layer.msg(res.msg, {icon: 2, anim: 6});
                    }
					return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
						notice_tb.reload();
					});
				},
				error: function () {
					return layer.msg("服务器出错啦", {icon: 5, anim: 6})
				}
			});
			layer.close(index);
		});
	});
	
	/**
	 * 搜索
	 */
	$("#btn_search").click(function(){
		layer.prompt({
			formType: 0,
			title:"请输入搜索关键字",
			btn:"搜索",
		}, function(value, index) {
			/* ajax 请求搜索 */
			notice_tb.reload({
				url: '/notice/search',
				where: {
					'keyword': value
				},
				page: {
					curr: 1, //从第一页开始
				}
			});
			layer.close(index);
		});
	});
});
