layui.use(['element', 'jquery', 'layer', 'form', 'table'], function() {
	var element = layui.element,
		$ = layui.jquery,
		form = layui.form,
		table = layui.table,
		layer = layui.layer;

	//渲染表格数据
	var user_tb = table.render({
		elem: '#dataTable',
		url: '/user/list',
		headers: {
			'Authorization': $.cookie("access_token")
		},
		toolbar: '#tableToolbar', //开启头部工具栏，并为其绑定左侧模板
		height: 550,
		cols: [
			[{
				type: 'checkbox',
				fixed: 'left'
			},
				{
					field: 'userId',
					title: '用户ID',
					width: 120,
					fixed: 'left',
					sort: true
				},
				{
					field: 'userName',
					title: '用户名',
					width: 120
				},
				{
					field: 'avatar',
					title: '头像',
					width: 80,
					templet: function(data) {
						return '<div><img src="'+data.avatar+'" width="35px" height="35px" style="border-radius:50%;"/></div>';
					}
				},
				{
					field: 'email',
					title: '邮箱',
					width: 150
				},
				{
					field: 'signature',
					title: '个性签名',
					width: 200
					
				},
				{
					field: 'isLock',
					title: '用户状态',
					width: 114,
					sort: true,
					templet: function(data) {
						return data.isLock?'<button class="layui-btn layui-btn-xs layui-btn-warm">锁定</button>':'<button class="layui-btn layui-btn-xs layui-btn-primary">正常</button>';
					}
				},
				{
					field: 'roleName',
					title: '角色类型',
					width: 120,
					sort: true,
					templet: function(data) {
						if (data.role.roleId == 2) {
							return '<button class="layui-btn layui-btn-xs btn-color-lgreen">' + data.role.roleName + '</button>';
						} else if (data.role.roleId == 3) {
							return '<button class="layui-btn layui-btn-xs layui-btn-normal">' + data.role.roleName + '</button>';
						} else {
							return '<button class="layui-btn layui-btn-xs layui-btn-primary">' + data.role.roleName + '</button>';
						}
					}
				},
				{
					field: 'joinTime',
					title: '加入时间',
					width: 200,
					sort: true
				},
				{
					fixed: 'right',
					title: '操作',
					toolbar: '#tableBar',
					width: 150
				}
			]
		],
		page: true,
		size:'lg'
	});

	//头工具栏事件
	table.on('toolbar(dataTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
			case 'deleteSelected':
				var data = checkStatus.data;
				layer.alert(JSON.stringify(data));
				break;
		};
	});

	//监听行工具事件
	table.on('tool(dataTable)', function(obj) {
		//获取编辑行数据
		var data = obj.data;
		// console.log(obj)
		if (obj.event === 'del') {
			layer.confirm('真的删除行么', function(index) {
				$.ajax({
					url: '/user/list/' + data.userId,
					type: 'delete',
					headers: {
						'Authorization': $.cookie("access_token")
					},
					dataType: 'json',
					success: function (res) {
						if (res.code != 0) {
                            return layer.msg(res.msg, {icon: 2, anim: 6});
                        }
						return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
							obj.del();
						});
					},
					error: function () {
						return layer.msg("服务器出错啦", {icon: 5, anim: 6})
					}
				});
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			layer.open({
				type: 1,
				content: $("#userEditPopup").html(),
				title: "编辑",
				btn: ['保存'],
				anim: 1,
				area: ['420px', '300px'],
				//保存
				yes: function(index) {
					data.roleId = $("#role").val();
					data["role"]["roleId"] = $("#role").val();
					data["role"]["roleName"] = $("#role :selected").text();

					data["isLock"] = $("#isLock")[0].checked;
					/* ajax请求服务端 */
					// console.log(data);
					$.ajax({
						url: '/user/list/' + data.userId,
						type: 'put',
						headers: {
							'Authorization': $.cookie("access_token")
						},
						data: JSON.stringify(data),
						contentType: 'application/json',
						dataType: 'json',
						success: function (res) {
							if (res.code != 0) {
                                return layer.msg(res.msg, {icon: 2, anim: 6});
                            }
							return layer.msg(res.msg, {icon: 1, anim: 1, time: 1500}, function () {
								// obj.update(data);
								user_tb.reload();
							});
						},
						error: function () {
							return layer.msg("服务器出错啦", {icon: 5, anim: 6})
						}
					});

					layer.close(index);
					// location.reload();
				},
				//弹窗弹出后执行的回调函数，可以用来初始化一些控件元素
				success: function() {
					//初始化下拉选项框的值
					//从数据库中获取角色列表
					$.getJSON("/role/list", function (result) {
                        var role = result.data;
                        for (let i = 0; i < role.length; i++) {
                            $("#role").append('<option value="' + role[i].roleId + '">' + role[i].roleName + '</option>');
                        }
                        //填充弹窗表单
                        $("#role").val(data.role.roleId);
                        $("#isLock").attr('checked', data.isLock);
                        //重新渲染样式
                        form.render();
                    });
				}
			});
		}
	});


	/**
	 * 搜索用户名
	 */
	$("#searchUserName").change(function () {
		var keyword = $("#searchUserName").val();
		//搜索到数据重新渲染表格
		user_tb.reload({
			url: '/user/search',
			where: {
				'keyword': keyword
			},
			page: {
				curr: 1, //从第一页开始
			}
		});
	});
});
