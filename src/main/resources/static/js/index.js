layui.use(['element', 'jquery', 'laypage', 'util', 'layer', 'laydate', 'laytpl', 'carousel'], function () {
    var element = layui.element,
        $ = layui.jquery,
        laypage = layui.laypage,
        util = layui.util,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        carousel = layui.carousel,
        layer = layui.layer;

    laydate.render({
		elem: '#calendar',
		position: 'static',
		btns: ['clear', 'now'],
		theme: '#5FB878',
		calendar: true,
        mark: {   //标记重要日子
            // '0-10-14': '生日'
            // ,'0-12-31': '跨年' //每年12月31日
            // ,'0-0-10': '工资' //每个月10号
            // ,'2017-8-15': '' //具体日期
            // ,'2017-8-20': '预发' //如果为空字符，则默认显示数字+徽章
            // ,'2017-8-21': '发布'
        },
    });

    //公告轮播
    var notice_broadcast = carousel.render({
        elem: '#notice-broadcast'
        , interval: 3500  //切换时间
        , anim: 'updown'  //上下切换,default（左右切换）,fade（渐隐渐显切换）
        , width: '100%'
        , height: '30px'
        , arrow: 'none'  //不显示箭头
        , indicator: 'none'  //不显示指示器
    });

    //获取公告信息
    $.getJSON("/notice/list?page=1&limit=5", function (res) {
        if (res.code != 0 || res.count == 0) {
            $("#notice-content").append('<div style="color: #ff0000;background-color: #ffffff">' +
                '<i class="fa fa-volume-up"></i>&nbsp;&nbsp;暂无数据</div>');
            return;
        }
        layui.each(res.data, function (index, item) {
            $("#notice-content").append('<div style="color: #ff0000;background-color: #ffffff">' +
                '<i class="fa fa-volume-up"></i>&nbsp;&nbsp;' + item.content + '</div>');
        });
        notice_broadcast.reload();
        return;
    });

    //关于博主信息卡片
    var tips;
    $('#qq').on({
        mouseover: function () {
            tips = layer.tips(
                '<img src="../img/qq.jpg" style="width:110px;height:110px;"/>',
                this, {
                    tips: 1,
                    time: 0
				})
		},
		mouseout: function() {
			layer.close(tips);
		}
	});
	$('#wechat').on({
		mouseover: function() {
			tips = layer.tips(
				'123456789',
				this, {
					tips: 1,
					time: 0
				})
		},
		mouseout: function() {
			layer.close(tips);
		}
	})
	$('#lm').on({
		mouseover: function() {
			tips = layer.tips('有什么想对我说嘛~', this, {
				tips: 1,
				time: 0
			})
		},
		mouseout: function() {
			layer.close(tips);
		}
	})
	$('#github').on({
		mouseover: function() {
			tips = layer.tips('去我的GitHUb~', this, {
				tips: 1,
				time: 0
			})
		},
		mouseout: function () {
			layer.close(tips);
		}
	})


	//获取最新的日记动态
	$.getJSON("/diary/list?page=1&limit=3", function (res) {
		// console.log(res);
		if (res.code != 0 || res.count == 0) {
			//出错或数据为0条时
			$("#latest-diary").html("暂无数据");
			return;
		}
		for (let i = 0; i < res.data.length; i++) {
			res.data[i].createTime = util.timeAgo(res.data[i].createTime);
		}
		//数据不为空时渲染数据
		return laytpl($("#contentTpl").html()).render(res.data, function (result) {
			//将渲染后的html插入到指定元素
			$("#latest-diary").before(result);
		});
	});

	/* 分页查询并显示 */
	function getRecommendArticlesByPage(curr, limit) {
		$.getJSON("/article/list", {type: 1, page: curr || 1, limit: limit || 10, isRecommend: true}, function (res) {
			if (res.code != 0) {
				return $("#index-cards").html(res.msg);
			}
			//拿到数据渲染模板
			$("#index-cards").html("");  //渲染新页码数据前清空div容器
			layui.each(res.data, function (index, item) {
				if (item.isTop) {
					//如果置顶，从指定元素顶部插入
					laytpl($("#index-articleCard").html()).render(item, function (html) {
						$("#index-cards").prepend(html);
					});
				} else {
					laytpl($("#index-articleCard").html()).render(item, function (html) {
						$("#index-cards").append(html);
					});
				}
			});
			console.log(res);
			laypage.render({
				elem: 'index-page-util',
				count: res.count,
				limit: limit || 10,
				curr: curr || 1,
				// layout: ['prev', 'next'],
				jump: function (obj, first) {
					if (!first) {     //点击跳页触发函数自身，并传递当前页：obj.curr
						getRecommendArticlesByPage(obj.curr, obj.limit);
					}
				}
			});
		});
	}

	getRecommendArticlesByPage(1, 6);

});
