layui.use(['element', 'jquery','util','layer','form','laytpl'], function() {
    var element = layui.element,
        $ = layui.jquery,
        form = layui.form,
        util=layui.util,
        laytpl=layui.laytpl,
        layer = layui.layer;



    //获取日记数据
    var getDate = function (page, limit) {
        $("#record-more").off("click");   //必须要清除$("#record-more")的click事件，以防止事件冒泡
        var queryString="";
        if (page != null && limit != null) {
            queryString="?page="+page+"&"+"limit="+limit;
        }
        $.getJSON("/diary/list"+queryString,function (res) {
            // console.log(res);
            if (res.code == 0) {
                //渲染数据总数
                $("#record-count").text(res.count);
               if(res.count!=0){
                   for (let i = 0; i < res.data.length; i++) {
                       res.data[i].createTime=util.toDateString(res.data[i].createTime,"yyyy年MM月dd日 HH:mm:ss");
                   }
                   //数据不为空时渲染数据
                   laytpl($("#contentTpl").html()).render(res.data,function (result) {
                       //将渲染后的html插入到指定元素后面(外部)
                       $("#timeline-bottom").before(result);
                   });
                   //判断当前页是否为页尾，不是则为#record-more绑定查询下一页事件
                   if(res.currPage<res.totalPage){
                       $("#record-more").on("click",function () {
                           getDate(res.currPage+1,res.limit);
                       });
                   }else{
                       $("#record-more").off("click").text("已经到尽头啦，难道你还想获取上古世纪的记录？");
                   }
               }else {
                   $("#record-more").text("暂时没有任何数据");
               }
            }
        });
    };

    getDate(1, 5);

});
