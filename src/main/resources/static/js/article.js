layui.use(['element', 'jquery', 'laypage', 'laytpl'], function () {
	var element = layui.element,
		$ = layui.jquery,
		laytpl = layui.laytpl,
		laypage = layui.laypage;

	/* 获取随机字体大小 */
	function getRandomFontSize(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	/* 获取随机颜色 */
	function getRandomColor() {
		var str = Math.ceil(Math.random() * 16777215).toString(16);
		if (str.length < 6) {
			str = "0" + str;
		}
		return str;
	}

	/* 获取分类信息 */
	$.getJSON("/category/list", function (res) {
		if (res.code != 0) {
			return $("#category").html("获取数据失败");
		}
		return layui.each(res.data, function (index, item) {
			$("#category").append('<a href="/article/category/' + item.categoryId + '">' + item.categoryName + '</a>')
		});
	});
	/* 获取标签信息 */
	$.getJSON("/tag/list", function (res) {
		if (res.code != 0) {
			return $("#tags").html("获取数据失败");
		}
		layui.each(res.data, function (index, item) {
			//拼接的Html后面需要添加 空格 或者 "\n"等，不然样式会出现问题
			$("#tags").append('<a href="/article/tag/' + item.tagId + '">' + item.tagName + '</a>\n');
		});
		/* 对首页标签板块着色 */
		var obj = $("#tags a");
		for (var i = 0; i < obj.length; i++) {
			obj[i].style.fontSize = getRandomFontSize(15, 22) + "px";
			obj[i].style.color = "#" + getRandomColor();
		}
	});

	/* 分页查询并显示 */
	function getArticlesByPage(param) {
		param["type"] = 1;
		param["page"] = param.page || 1;
		param["limit"] = param.limit || 10;
		$.getJSON("/article/list", param, function (res) {
			if (res.code != 0) {
				return $("#cards").html(res.msg);
			}
			//拿到数据渲染模板
			laytpl($("#articleCard").html()).render(res.data, function (html) {
				$("#cards").html(html);
			});
			// console.log(res);
			laypage.render({
				elem: 'page-util',
				count: res.count,
				limit: param.limit || 10,
				curr: param.page || 1,
				// layout: ['prev', 'next'],
				jump: function (obj, first) {
					if (!first) {     //点击跳页触发函数自身，并传递当前页：obj.curr
						getArticlesByPage({page: obj.curr, limit: obj.limit});
					}
				}
			});
		});
	}

	getArticlesByPage({page: 1, limit: 6});


	//搜索
	$("#btn_search").click(function () {
		var value = $("#s_keyword").val();
		getArticlesByPage({title: value});
	});


});
