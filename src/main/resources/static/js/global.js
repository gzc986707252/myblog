layui.use(['element', 'jquery', 'layer', 'form', 'laytpl'], function () {
    var element = layui.element,
        laytpl = layui.laytpl,
        $ = layui.jquery;

    window.logout=function(){
        //由于后端响应cookie时设置了path，因此删除时也要指定path,否则部分页面删除cookie失败
        // $.removeCookie("access_token",{ path: '/' });
        document.cookie = "access_token=; expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
        // $.removeCookie("uid",{ path: '/' });
        document.cookie = "uid=; expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
        window.location.reload();
    }

    //获取用户信息并显示在导航栏
    $.ajax({
        url: '/user/getLoggedInUserInfo',
        type: 'get',
        headers: {
            'Authorization': $.cookie("access_token")
        },
        dataType: 'json',
        success: function (res) {
            if (res.code == 0) {
                //成功获得数据渲染模板
                laytpl($("#user-info-tpl").html()).render(res.data, function (html) {
                    $("#user-info-nav").html(html);
                });
                //重新渲染导航条
                element.render('nav');
                //绑定退出按钮
            }
        }
    });


    /* 向下滚动时导航条固定 */
    var navOffset = $(".blog-header").offset().top;
    $(window).scroll(function () {
        var scrollPos = $(window).scrollTop();
        if (scrollPos > navOffset) {
            $(".blog-header").addClass("fixed");

        } else {
            $(".blog-header").removeClass("fixed");
        }
    });


    //移动端点击显示侧边导航栏
    $("#mobile_show_nav").click(function () {
        $("#nav_left").addClass("layui-nav-tree layui-nav-side mobile-nav-left").show().animate({left: '0px'});
        $(".shadow").show();
        //禁止页面滚动
        var top = $(document).scrollTop();
        $(document).scroll(function () {
            $(document).scrollTop(top);
        });
        $(".shadow").on("touchmove", function (event) {
            event.preventDefault()
        }, false)
    });

    //点击遮罩层收回侧边导航栏
    $(".shadow").click(function () {
        $("#nav_left").removeClass("layui-nav-tree layui-nav-side mobile-nav-left").hide();
        $(".shadow").hide();
        $(document).unbind("scroll");
        $(".shadow").unbind("touchmove");
    });
});
