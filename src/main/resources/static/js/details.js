layui.use(['element', 'layedit', 'jquery', 'layer', 'form'], function() {
    var element = layui.element,
        layedit = layui.layedit,
        $ = layui.jquery,
        form = layui.form,
        layer = layui.layer;


//构建一个默认的编辑器
    var index = layedit.build('blog-comment', {
        height: 150,
        tool: ['strong','italic','underline','del','|', 'face','|','link','unlink']
    });

// alert(layedit.getContent(index)); //获取编辑器内容
//提交评论或留言
    $('#btn-submit-comment').on('click', function() {

        layer.alert(layedit.getContent(index));
    });


//评论点击回复按钮的事件
    window.reply = function(toUserId, toUserName) {
        var reply_idex;
        layer.open({
            type: 1,
            content: '<div style="margin:10px"><textarea class="layui-textarea" id="reply" style="display: none;"></textarea></div>',
            title: "正在回复" + toUserName,
            btn: '提交',
            anim: 1,
            area: ['350px', '270px'],
            maxmin: true,
            yes: function() {
                layer.msg(toUserId + ":" + toUserName+"说："+layedit.getContent(reply_idex),{icon: 6});
            },
            success: function() {
                //初始化回复文本框
                reply_idex=layedit.build('reply', {
                    height: 100,
                    tool: ['face','link']
                });
            }
        });
    };

});
