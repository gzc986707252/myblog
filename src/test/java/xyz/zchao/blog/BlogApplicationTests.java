package xyz.zchao.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import xyz.zchao.blog.pojo.Comment;
import xyz.zchao.blog.service.CommentService;
import xyz.zchao.blog.service.DiaryService;
import xyz.zchao.blog.service.UserService;

import java.util.List;

@SpringBootTest
class BlogApplicationTests {

    @Autowired
    private DiaryService diaryService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;

    @Test
    void contextLoads() {
       System.out.println(diaryService.retrieveAllByPaging(null,null));
    }

    @Test
    void testUserService() {
        System.out.println(userService.findUserByUserName("admin"));
    }

    @Test
    void testCommentService(){
        List<Comment> comments = commentService.getParentComments(1);
        comments.stream().forEach(comment -> {
            System.out.println(comment);
        });
    }

}
