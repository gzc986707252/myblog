package xyz.zchao.blog.service;

import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.parameter.BaseParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;

import javax.swing.plaf.synth.SynthSpinnerUI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 22:49
 */
@SpringBootTest
public class ArticleServiceTest {

    @Autowired
    private ArticleService articleService;


    @Test
    public void test3() {
        ArticleParam param = new ArticleParam();
        param.setTitle("s");
        param.setPage(null);
        param.setLimit(null);
        List<Article> articles = articleService.findArticlesByOptionalParameters(param);
        articles.stream().forEach(article -> {
            System.out.println(article);
        });
        PageInfo<Article> pageInfo = new PageInfo<>(articles);
        System.out.println(pageInfo);

    }

    @Test
    public void test1() {
        BaseParam param = new BaseParam();
        System.out.println(param);
    }
}
