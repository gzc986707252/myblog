package xyz.zchao.blog.dao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import xyz.zchao.blog.pojo.Category;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/19 20:53
 */
@SpringBootTest
public class CategoryMapperTest {
    @Resource
    private CategoryMapper categoryMapper;

    @Test
    public void test() {

        PageHelper.startPage(-1, 0);
        List<Category> categories = categoryMapper.selectByCategoryName("s");
        categories.stream().forEach(category -> {
            System.out.println(category);
        });
        PageInfo<Category> categoryPageInfo = new PageInfo<>(categories);
        System.out.println(categoryPageInfo.toString());

    }
}
