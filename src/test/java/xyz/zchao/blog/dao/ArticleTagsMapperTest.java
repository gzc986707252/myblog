package xyz.zchao.blog.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import xyz.zchao.blog.pojo.Tag;

import javax.annotation.Resource;
import java.util.List;


/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 19:17
 */
@SpringBootTest
public class ArticleTagsMapperTest {
    @Resource
    private ArticleTagsMapper articleTagsMapper;

    @Test
    public void test() {
        Assert.isTrue(1 == articleTagsMapper.insert(1, 1), "error");
        Assert.isTrue(1 == articleTagsMapper.insert(1, 2), "error");
        Assert.isTrue(1 == articleTagsMapper.insert(1, 3), "error");
        Assert.isTrue(1 == articleTagsMapper.insert(2, 1), "error");
        Assert.isTrue(1 == articleTagsMapper.insert(2, 2), "error");
        Assert.isTrue(1 == articleTagsMapper.insert(2, 3), "error");
    }

    @Test
    public void test1() {
        List<Tag> tags2 = articleTagsMapper.selectTagsByArticleId(2);
        tags2.stream().forEach(tag -> {
            System.out.println(tag);
        });
        System.out.println("-----------------------\n");
        List<Tag> tags3 = articleTagsMapper.selectTagsByArticleId(3);
        tags3.stream().forEach(tag -> {
            System.out.println(tag);
        });
        System.out.println("-----------------------\n");
        List<Tag> tags1 = articleTagsMapper.selectTagsByArticleId(1);
        tags1.stream().forEach(tag -> {
            System.out.println(tag);
        });
    }

    @Test
    public void test2() {
        articleTagsMapper.deleteTagsByArticleId(6);
    }
}
