package xyz.zchao.blog.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import xyz.zchao.blog.parameter.ArticleParam;
import xyz.zchao.blog.pojo.Article;
import xyz.zchao.blog.pojo.ArticleWithBLOBs;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/5/17 19:47
 */
@SpringBootTest
public class ArticleMapperTest {
    @Resource
    private ArticleMapper articleMapper;

    @Test
    public void test1() {
        ArticleWithBLOBs article2 = articleMapper.selectByPrimaryKeyWithBLOBs(3);
        System.out.println(article2);
        ArticleWithBLOBs article3 = articleMapper.selectByPrimaryKeyWithBLOBs(3);
        System.out.println(article3);
        Article article = articleMapper.selectByPrimaryKey(3);
        System.out.println(article);
        Article article1 = articleMapper.selectByPrimaryKey(5);
        System.out.println(article1);
    }


    @Test
    public void test4() {
        ArticleParam param = new ArticleParam();
        param.setIsOriginal(false);
        param.setPage(1);
        System.out.println(param);
        List<Article> articles = articleMapper.selectArticlesByOptionalParameters(param);
        for (Article article : articles) {
            System.out.println(article);
        }
/*        System.out.println("---------------------------\n");
        List<ArticleWithBLOBs> articleWithBLOBs = articleMapper.selectArticlesByOptionalParametersWithBLOBs(param);
        articleWithBLOBs.stream().forEach(item -> {
            System.out.println(item);
        });*/
    }

}
